FROM python:3.9-slim-buster
LABEL description="python_webcounter"
LABEL maintainer="nunoserodio.pinto@natixis.com"
Label version="1.0.1"
ARG REDIS_URL='localhost'
ENV REDIS_URL=${REDIS_URL}
WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["python", "app.py"]
