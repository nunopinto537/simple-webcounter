# Webcounter

Simple Python Webcounter with redis 

## Build
docker build -t webcounter .

## Dependencies
docker run -d  --name redis --rm redis

## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis webcounter

## Git Lab runner
gitlab-runner register -n
--url https://gitlab.com/
--executor shell
--description "docker-lab"
--tag-list "production"
--registration-token GR1348941K3AyYyEnyyfzwrc89TvJ